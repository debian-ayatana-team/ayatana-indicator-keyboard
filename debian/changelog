ayatana-indicator-keyboard (24.7.2-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 02 Mar 2025 17:48:25 +0100

ayatana-indicator-keyboard (24.7.1-1) unstable; urgency=medium

  * New upstream release.
    - Drop obsolete .pkla file. (Closes: #1093061).
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 23 Feb 2025 18:23:28 +0100

ayatana-indicator-keyboard (24.7.0-3) unstable; urgency=medium

  * debian/control:
    + Move from R: to S: matekbd-keyboard-display | gkbd-capplet. These should
      get pulled in by the desktop environments that use AyatanaIndicators.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 24 Jan 2025 10:39:13 +0100

ayatana-indicator-keyboard (24.7.0-2) unstable; urgency=medium

  * debian/rules:
    + Set ENABLE_UBUNTU_ACCOUNTSSERVICE=ON for builds against Ubuntu and
      derivatives.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 12 Jul 2024 16:23:18 +0200

ayatana-indicator-keyboard (24.7.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 10 Jul 2024 14:08:59 +0200

ayatana-indicator-keyboard (24.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add to R: matekbd-keyboard-display | gkbd-capplet.
    + Bump Standards-Version to 4.7.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 22 May 2024 09:03:03 +0200

ayatana-indicator-keyboard (24.2.0-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 24 Feb 2024 12:18:14 +0100

ayatana-indicator-keyboard (23.10.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.
    + In B-D:, switch from systemd to systemd-dev. (Closes: #1060470).
  * debian/patches:
    + Add patch header to 2001_use-keyboard-icon.patch.
  * lintian: Drop library-not-linked-against-libc from lintian overrides. Not
    getting triggered anymore.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 05 Feb 2024 15:59:37 +0100

ayatana-indicator-keyboard (22.9.1-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001-Try-to-place-the-indicator-in-the-leftmost-position-.patch.
      Try to place indicator correctly in Lomiri.
    + Add 2001_use-keyboard-icon.patch. Also use keyboard icon (and not the
      language code) on desktops.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 28 Feb 2023 22:30:05 +0100

ayatana-indicator-keyboard (22.9.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update attributions for debian/.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Jan 2023 08:17:12 +0100

ayatana-indicator-keyboard (22.9.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 02 Oct 2022 12:51:39 +0200

ayatana-indicator-keyboard (22.2.3-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 15 Jul 2022 22:51:55 +0200

ayatana-indicator-keyboard (22.2.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Add B-Ds: libaccountsservice-dev, libxkbcommon-dev (>=1.0.3), and
      libxkbregistry-dev (>=1.0.3).
    + Relax versioned B-D on libxklavier-dev (5.4 -> 5.3).
  * debian/ayatana-indicator-keyboard.links:
    + Drop file, not requires for DH compat level 12 and above.
  * debian/ayatana-indicator-keyboard.lintian-overrides:
    + Adjust link-to-shared-library-in-wrong-package override phrasing.
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Jun 2022 06:50:25 +0200

ayatana-indicator-keyboard (22.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update auto-generated copyright in file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 18 Feb 2022 17:02:00 +0100

ayatana-indicator-keyboard (0.9.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/upstream/metadata:
    + Update points of contact, put UBports Foundation in Donation: field.
  * debian/copyright:
    + Add auto-generated copyright.in reference file for tracking copyright
      changes in later uploads.
    + Update copyright attributions.
  * debian/control:
    + Add to B-D: libayatana-common-dev (>= 0.9.3).
    + Bump Standards-Version: 4.6.0. No changes needed.
    + Update LONG_DESCRIPTION, fix grammar issues, etc.
  * debian/ayatana-indicator-keyboard.lintian-overrides:
    + Override warnings/errors regarding the dynamically loaded library
      modules.
  * debian/rules:
    + Adjust for upstream release 0.9.0.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 19 Nov 2021 22:29:20 +0100

ayatana-indicator-keyboard (0.7.901-2) unstable; urgency=medium

  * debian/changelog:
    + Post-upload add closure for LP:#1915492
  * debian/control:
    + Fix Vcs-*: URLs. Point them to the packaging Git on salsa.debian.org.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 23 May 2021 10:20:27 +0200

ayatana-indicator-keyboard (0.7.901-1) unstable; urgency=medium

  * New upstream release.
    - Test if layout variant is NULL before checking its string length.
      (LP: #1915492)

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 14 Apr 2021 11:31:17 +0200

ayatana-indicator-keyboard (0.7.900-3) unstable; urgency=medium

  * debian/copyright:
    + Adjust GPL-3 license text to be really GPL-3 (and not GPL-3+).
      Thanks to Thorsten Alteholz for spotting this.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Sun, 07 Feb 2021 15:47:28 +0100

ayatana-indicator-keyboard (0.7.900-2) unstable; urgency=medium

  * debian/copyright:
    + Revisit copyright attributions and fix license (GPL-3+ -> GPL-3). Also
      spot other copyright holders for some files. Thanks to Thorsten Alteholz
      for spotting these.
  * Initial release to Debian. (Closes: #981645).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 04 Feb 2021 23:11:41 +0100

ayatana-indicator-keyboard (0.7.900-1) REJECTED; urgency=medium

  * Initial release to Debian. (Closes: #981645).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 02 Feb 2021 16:13:31 +0100
